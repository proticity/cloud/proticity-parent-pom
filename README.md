# Proticity Parent POM
This is a parent POM for all Proticity Java projects with common configuration needed for all of them. It is not a
general-purpose Java project POM; for that see [Java-Next POM](https://gitlab.com/proticity/cloud/java-next-pom). This
project is an extension of the Java-Next POM with configuration customized for Proticity projects.